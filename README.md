# M2MBD: Model II Magic Bus Device

M2MBD is an expansion card for the Tandy/TRS-80 Model II, 12, 16, and 6000 computers. This repository contains the printed circuit board design for this card. A working M2MBD card also requires FPGA configuration and ESP32 module firmware, which are provided in other repositories.

