EESchema Schematic File Version 4
LIBS:m2mbd-cache
EELAYER 26 0
EELAYER END
$Descr B 17000 11000
encoding utf-8
Sheet 1 1
Title "Model II Magic Bus Device"
Date "2018-12-22"
Rev "DRAFT"
Comp "Mark J. Blair <nf6x@nf6x.net>"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Module:TRS-80_Model-II_CardEdge P1
U 1 1 5C1F1E51
P 7550 3050
F 0 "P1" H 8250 3340 60  0000 C CNN
F 1 "TRS-80_Model-II_CardEdge" H 8250 3234 60  0000 C CNN
F 2 "Connector_Module:TRS-80_Model-II_CardEdge" H 7750 -1050 60  0001 L CNN
F 3 "" H 7550 3050 50  0001 C CNN
F 4 "DNI" H 7750 -1150 50  0001 L CNN "IPN"
F 5 "DNI:DNI" H 7750 -1250 50  0001 L CNN "MPN"
	1    7550 3050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5C1F2499
P 7350 7150
F 0 "#PWR?" H 7350 6900 50  0001 C CNN
F 1 "GND" H 7355 6977 50  0000 C CNN
F 2 "" H 7350 7150 50  0001 C CNN
F 3 "" H 7350 7150 50  0001 C CNN
	1    7350 7150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5C1F24C5
P 9150 7150
F 0 "#PWR?" H 9150 6900 50  0001 C CNN
F 1 "GND" H 9155 6977 50  0000 C CNN
F 2 "" H 9150 7150 50  0001 C CNN
F 3 "" H 9150 7150 50  0001 C CNN
	1    9150 7150
	1    0    0    -1  
$EndComp
Wire Wire Line
	9150 7150 9150 6950
Wire Wire Line
	9150 3150 8950 3150
Wire Wire Line
	8950 3350 9150 3350
Connection ~ 9150 3350
Wire Wire Line
	9150 3350 9150 3150
Wire Wire Line
	8950 6450 9150 6450
Connection ~ 9150 6450
Wire Wire Line
	9150 6450 9150 5450
Wire Wire Line
	8950 5450 9150 5450
Connection ~ 9150 5450
Wire Wire Line
	9150 5450 9150 3350
Wire Wire Line
	8950 6950 9150 6950
Connection ~ 9150 6950
Wire Wire Line
	9150 6950 9150 6650
Wire Wire Line
	7350 7150 7350 6950
Wire Wire Line
	7350 3150 7550 3150
Wire Wire Line
	7550 3350 7350 3350
Connection ~ 7350 3350
Wire Wire Line
	7350 3350 7350 3150
Wire Wire Line
	7550 5450 7350 5450
Connection ~ 7350 5450
Wire Wire Line
	7350 5450 7350 3350
Wire Wire Line
	7550 6450 7350 6450
Connection ~ 7350 6450
Wire Wire Line
	7350 6450 7350 5450
Wire Wire Line
	8950 6650 9150 6650
Connection ~ 9150 6650
Wire Wire Line
	9150 6650 9150 6450
Wire Wire Line
	7550 6650 7350 6650
Connection ~ 7350 6650
Wire Wire Line
	7350 6650 7350 6450
Wire Wire Line
	7550 6950 7350 6950
Connection ~ 7350 6950
Wire Wire Line
	7350 6950 7350 6650
$EndSCHEMATC
